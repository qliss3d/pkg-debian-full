// This is Free Software
// See the GNU General Public License @ http://www.gnu.org/copyleft/gpl.html
// for details about the terms and conditions for using and distributing
// this piece of software.
// This software comes with absolutely NO WARRANTY!
// (c) 2003 by Daniel Gruen <daniel_gruen@web.de>

#include <qwidget.h>
#include <q3vbox.h>
//Added by qt3to4:
#include <QMouseEvent>
#include <QKeyEvent>
#include <QPaintEvent>

const double pi = 3.14159265359;
const double pi180 = pi/180.0;

struct dddcoord {
   float x;
   float y;
   float z;
};

struct ddcoord {
   float x;
   float y;
};

class QLiss3D : public QWidget {
	Q_OBJECT

    public:
	QLiss3D(QWidget *parent=0, const char *name = 0);

    private:
	int   xang,yang;
	int   ppf;
	int   k;
	int   cr,cg,cb;
	float b,c,d,df,xp,yp,zp;
	bool  ls,is;
	int   msx,msy;
	bool  really_repaint;
	Q3VBox *help;

    public slots:
	void  setXAngle(int degrees);
	void  setYAngle(int degrees);
	void  setK(int dk);
	void  setPPF(int dppf);
	void  setColorR(int dcr);
	void  setColorG(int dcg);
	void  setColorB(int dcb);
	void  setXFreq(int db){if(b != db){b = db; emit xFreqChanged(db);}}
	void  setYFreq(int dc){if(c != dc){c = dc; emit yFreqChanged(dc);}}
	void  setZFreq(int dd){if(d != dd){d = dd; emit zFreqChanged(dd);}}
	void  setXPhase(float dxp){if(xp!=dxp){xp=dxp; emit xPhaseChanged(dxp);}}
	void  setYPhase(float dyp){if(yp!=dyp){yp=dyp; emit yPhaseChanged(dyp);}}
	void  setZPhase(float dzp){if(zp!=dzp){zp=dzp; emit zPhaseChanged(dzp);}}
	void  setLineState(int state){if(state==2)ls=1; if(state==0)ls=0; repaint(FALSE);}
	void  setInfoState(int state){if(state==2)is=1; if(state==0)is=0; repaint(FALSE);}
	void  randomFigure();
	void  setDensity();
	void  repaintMe(){if(really_repaint){repaint(FALSE);}}
	void  playWav();

    signals:
	void  xAngleChanged(int);
	void  yAngleChanged(int);
	void  colorRChanged(int);
	void  colorGChanged(int);
	void  colorBChanged(int);
	void  xFreqChanged(int);
	void  yFreqChanged(int);
	void  zFreqChanged(int);
	void  xPhaseChanged(float);
	void  yPhaseChanged(float);
	void  zPhaseChanged(float);
	void  kChanged(int);
	void  PPFChanged(int);
	void  densityChanged(float);

    protected:
	void paintEvent(QPaintEvent *);
	void keyPressEvent(QKeyEvent * e);
	void mousePressEvent(QMouseEvent * e);
	void mouseMoveEvent(QMouseEvent * e);
};

class QLiss3DObject : public Q3VBox {
    Q_OBJECT
    public:
	QLiss3DObject(QWidget *parent=0, const char *name = 0);
};


/**************************************************************************
                   fmval.cpp  -  central computing function
                            -------------------
    begin                : January 03rd 2003
    copyright            : (C) 2003 by Daniel Gruen
    email                : daniel_gruen@web.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// this is a slim and special fms version for qliss3d
// please use the original version from http://sourceforge.net/projects/fmsynth
// if you want to do anything on it

#include "include/fmval.h"
#include "include/version.h"
#include "include/fmifstream.h"

#include <iostream>
#include <cmath>

using namespace std;

float   fmval(FMData *f, float atime){
  int v = 0;

 	while(v < f->getVlen() && f->vtl[v].getTime() <= atime) {
		v++;
	}

	switch(f->vtl[v-1].getLinetype()){

	    case 0:
		return -1;  // just do nothing

	    case 2:
	/* linetype 2: sine, linear beginning */
		return f->vtl[v-1].getValue() +
		       sin((atime - f->vtl[v-1].getTime()) / (f->vtl[v].getTime() - f->vtl[v-1].getTime()) * 90.0 * PI180) *
					 (f->vtl[v].getValue() - f->vtl[v-1].getValue());

	    case 3:
	/* linetype 3: sine, curved beginning */
		return f->vtl[v].getValue() +
		       sin(((atime - f->vtl[v-1].getTime()) / (f->vtl[v].getTime() - f->vtl[v-1].getTime()) * 90.0 + 90.0) * PI180) *
					 (f->vtl[v-1].getValue() - f->vtl[v].getValue());
	}

	if(debug_level)
		cout << "linetype of " << v-1 << " (" << int(f->vtl[v-1].getLinetype()) << ") is wrong!" << endl;
	return -2; // wrong linetype
}

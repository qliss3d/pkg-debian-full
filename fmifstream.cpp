/**************************************************************************
           fmifstream.cpp  -  providing fms-file reading utility
                             -------------------
    begin                : April 10th 2002
    copyright            : (C) 2002-2003 by Daniel Gruen
    email                : daniel_gruen@web.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// this is a slim and special fms version for qliss3d
// please use the original version from http://sourceforge.net/projects/fmsynth
// if you want to do anything on it

#include "include/fmplayer.h"

#ifdef FMS_WORKS

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "include/fmifstream.h"
#include "include/version.h"

using namespace std;

void FMData::init() {
	vtl = new VTL[5];
	vtl[0].setTime(0);
	vtl[0].setValue(128);
	vtl[0].setLinetype(2);

	vtl[1].setTime(100);
	vtl[1].setValue(255);
	vtl[1].setLinetype(3);

	vtl[2].setTime(200);
	vtl[2].setValue(128);
	vtl[2].setLinetype(2);

	vtl[3].setTime(300);
	vtl[3].setValue(0);
	vtl[3].setLinetype(3);

	vtl[4].setTime(400);
	vtl[4].setValue(128);
	vtl[4].setLinetype(0);

	mode = 1;
	vlen = 5;
}

#endif


// This is Free Software
// See the GNU General Public License @ http://www.gnu.org/copyleft/gpl.html
// for details about the terms and conditions for using and distributing
// this piece of software.
// This software comes with absolutely NO WARRANTY!
// (c) 2003 by Daniel Gruen <daniel_gruen@web.de>

#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>

#include <qapplication.h>
#include <qmessagebox.h>
#include <qtranslator.h>
#include <qtextcodec.h>

#include "qliss3d.h"

using namespace std;

int main(int argc, char * argv[]) {

	srand(time((time_t *)NULL));	//Zufall initialisieren

	QApplication a(argc, argv);
	QTranslator translator(0);
	translator.load(QString("qliss3d.") + QTextCodec::locale(), DATADIR );
	a.installTranslator(&translator);
	QLiss3DObject i(0,"Lissobjekt");

	a.setMainWidget(&i);

	i.show();

	QMessageBox::information(&i, QLiss3DObject::tr("About qliss3d-1.4"),QLiss3DObject::tr("This is Free Software licensed under the terms and conditions of the Gnu GPL.\nAsk me <daniel_gruen@web.de> any questions about qliss3D. Press F1 for help."));
		
	return a.exec();

}


<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>QLiss3D</name>
    <message>
        <source>turn figure</source>
        <translation>troi&apos;r ffigur</translation>
    </message>
    <message>
        <source>reset angle</source>
        <translation>ail-osod yr ongl</translation>
    </message>
    <message>
        <source>change red color value</source>
        <translation>newid gwerth y lliw coch</translation>
    </message>
    <message>
        <source>change green color value</source>
        <translation>newid gwerth y lliw gwyrdd</translation>
    </message>
    <message>
        <source>change blue color value</source>
        <translation>newid gwerth y lliw glas</translation>
    </message>
    <message>
        <source>zoom</source>
        <translation>chwyddo</translation>
    </message>
    <message>
        <source>change density</source>
        <translation>newid dwysedd</translation>
    </message>
    <message>
        <source>x phase shift</source>
        <translation>symudiad cydwedd x</translation>
    </message>
    <message>
        <source>y phase shift</source>
        <translation>symudiad cydwedd y</translation>
    </message>
    <message>
        <source>z phase shift</source>
        <translation>symudiad cydwedd z</translation>
    </message>
    <message>
        <source>toggle information</source>
        <translation>dangos/cuddio gwybodaeth</translation>
    </message>
    <message>
        <source>cursor keys, mouse dragging</source>
        <translation>allweddau cyrchydd, tynnu efo&apos;r llygoden</translation>
    </message>
    <message>
        <source>page up / page down</source>
        <translation>tudalen i fyny / tudalen i lawr</translation>
    </message>
    <message>
        <source>r / t</source>
        <translation>r / t</translation>
    </message>
    <message>
        <source>g / h</source>
        <translation>g / h</translation>
    </message>
    <message>
        <source>b / n</source>
        <translation>b / n</translation>
    </message>
    <message>
        <source>+ / -</source>
        <translation>+ / -</translation>
    </message>
    <message>
        <source>Ins / Del</source>
        <translation>Ins / Del</translation>
    </message>
    <message>
        <source>y / x</source>
        <translation>y / x</translation>
    </message>
    <message>
        <source>a / s</source>
        <translation>a / s</translation>
    </message>
    <message>
        <source>q / w</source>
        <translation>q / w</translation>
    </message>
    <message>
        <source>i</source>
        <translation>i</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>G&amp;adael</translation>
    </message>
    <message>
        <source>x freq.</source>
        <translation>amledd x</translation>
    </message>
    <message>
        <source>y freq.</source>
        <translation>amledd y</translation>
    </message>
    <message>
        <source>z freq.</source>
        <translation>amledd z</translation>
    </message>
    <message>
        <source>x angle</source>
        <translation>ongl x</translation>
    </message>
    <message>
        <source>y angle</source>
        <translation>ongl y</translation>
    </message>
    <message>
        <source>x phase</source>
        <translation>cydwedd x</translation>
    </message>
    <message>
        <source>y phase</source>
        <translation>cydwedd y</translation>
    </message>
    <message>
        <source>z phase</source>
        <translation>cydwedd z</translation>
    </message>
</context>
<context>
    <name>QLiss3DObject</name>
    <message>
        <source>About qliss3d-1.4</source>
        <translation type="obsolete">Ynghylch qliss3d-1.4</translation>
    </message>
    <message>
        <source>This is Free Software licensed under the terms and conditions of the Gnu GPL.
Ask me &lt;daniel_gruen@web.de&gt; any questions about qliss3D. Press F1 for help.</source>
        <translation>Mae hyn yn Feddalwedd Rhydd wedi ei thrwyddedu dan termau ac amodau&apos;r GPL GNU.
Gofynnwch unrhyw cwestiwn sydd gennych ynghylch qliss3D wrthyf fi &lt;daniel_gruen@web.de&gt;.
Gwthiwch F1 am gymorth.</translation>
    </message>
    <message>
        <source>Angle:</source>
        <translation>Ongl:</translation>
    </message>
    <message>
        <source>Points:</source>
        <translation>Pwyntiau:</translation>
    </message>
    <message>
        <source>x-frequency:</source>
        <translation>amledd-x:</translation>
    </message>
    <message>
        <source>y-frequency:</source>
        <translation>amledd-y:</translation>
    </message>
    <message>
        <source>z-frequency:</source>
        <translation>amledd-z:</translation>
    </message>
    <message>
        <source>&amp;Random</source>
        <translation>&amp;Hap</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Cysylltu</translation>
    </message>
    <message>
        <source>&amp;Play</source>
        <translation>Ch&amp;warae</translation>
    </message>
    <message>
        <source>About qliss3d-1.3</source>
        <translation type="unfinished">Ynghylch qliss3d-1.3 {3d?}</translation>
    </message>
</context>
</TS>

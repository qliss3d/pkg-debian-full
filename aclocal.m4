# Check for Qt compiler flags, linker flags, and binary packages
AC_DEFUN([CHECK_QT4],
[
AC_REQUIRE([AC_PROG_CXX])
AC_REQUIRE([AC_PATH_X])

AC_MSG_CHECKING([QTDIR])
AC_ARG_WITH([qtdir], [  --with-qtdir=DIR        Qt installation directory [default=$QTDIR]], QTDIR=$withval)
# Check that QTDIR is defined or that --with-qtdir given
if test x"$QTDIR" = x ; then
        # some usual Qt-locations
        QT_SEARCH="/usr /usr/lib/qt /usr/X11R6 /usr/local/Trolltech/Qt-4.0.0 /usr/local/Trolltech/Qt-4.0.1 /usr/local/Trolltech/Qt-4.1.0 /usr/local/Trolltech/Qt-4.2.0 /usr/local/Trolltech/Qt-4.2.1 /usr/local/Trolltech/Qt-4.2.2"
else
        QT_SEARCH=$QTDIR
        QTDIR=""
fi
for i in $QT_SEARCH ; do
        QT_INCLUDE_SEARCH="include/qt4 include"
        for j in $QT_INCLUDE_SEARCH ; do
                if test -f $i/$j/Qt/qglobal.h -a x$QTDIR = x ; then
                        QTDIR=$i
                        QT_INCLUDES=$i/$j
                fi
        done
done
if test x"$QTDIR" = x ; then
        AC_MSG_ERROR([*** QTDIR must be defined, or --with-qtdir option given])
fi
AC_MSG_RESULT([$QTDIR])

# Change backslashes in QTDIR to forward slashes to prevent escaping
# problems later on in the build process, mainly for Cygwin build
# environment using MSVC as the compiler
QTDIR=`echo $QTDIR | sed 's/\\\\/\\//g'`

AC_MSG_CHECKING([Qt includes])
# Check where includes are located
if test x"$QT_INCLUDES" = x ; then
        AC_MSG_ERROR([*** could not find Qt-includes! Make sure you have the Qt-devel-files installed!])
fi
AC_MSG_RESULT([$QT_INCLUDES])


if test -z "$QTHOSTDIR" ; then
        case "${prefix}" in
                /opt/mingw*)
                        QTHOSTDIR=/usr
                        ;;
                *)
                        if test -n "$QTDIR" ; then
                                QTHOSTDIR="$QTDIR"
                        else
                                QTHOSTDIR=/usr
                        fi
                        ;;
        esac
fi


# Check that moc is in path
AC_CHECK_PROG(MOC, moc-qt4, $QTHOSTDIR/bin/moc-qt4,,$QTHOSTDIR/bin/)
if test x$MOC = x ; then
        AC_CHECK_PROG(MOC, moc, $QTHOSTDIR/bin/moc,,$QTHOSTDIR/bin/)
        if test x$MOC = x ; then
        AC_MSG_ERROR([*** not found! Make sure you have Qt-devel-tools installed!])
        fi
fi

# Check that uic is in path
AC_CHECK_PROG(UIC, uic-qt4, $QTHOSTDIR/bin/uic-qt4,,$QTHOSTDIR/bin/)
if test x$UIC = x ; then
        AC_CHECK_PROG(UIC, uic, $QTHOSTDIR/bin/uic,,$QTHOSTDIR/bin/)
        if test x$UIC = x ; then
        AC_MSG_ERROR([*** not found! Make sure you have Qt-devel-tools installed!])
        fi
fi

# lupdate is the Qt translation-update utility.
AC_CHECK_PROG(LUPDATE, lupdate-qt4, $QTHOSTDIR/bin/lupdate-qt4,,$QTHOSTDIR/bin/)
if test x$LUPDATE = x ; then
        AC_CHECK_PROG(LUPDATE, lupdate, $QTHOSTDIR/bin/lupdate,,$QTHOSTDIR/bin/)
        if test x$MOC = x ; then
                AC_MSG_WARN([*** not found! It's not needed just for compiling but should be part of a proper Qt-devel-tools-installation!])
        fi
fi

# lrelease is the Qt translation-release utility.
AC_CHECK_PROG(LRELEASE, lrelease-qt4, $QTHOSTDIR/bin/lrelease-qt4,,$QTHOSTDIR/bin/)
if test x$LRELEASE = x ; then
        AC_CHECK_PROG(LRELEASE, lrelease, $QTHOSTDIR/bin/lrelease,,$QTHOSTDIR/bin/)
        if test x$MOC = x ; then
                AC_MSG_WARN([*** not found! It's not needed just for compiling but should be part of a proper Qt-devel-tools-installation!])
        fi
fi

# Calculate Qt include path
QT_CXXFLAGS=`pkg-config --cflags Qt3Support QtCore QtXml QtNetwork`

# On unix, figure out if we're doing a static or dynamic link
case "${host}" in
      *mingw32)
        QT_IS_STATIC=`ls $QTDIR/lib/*.a 2> /dev/null`
        if test "x$QT_IS_STATIC" = x; then
            QT_IS_STATIC="no"
        else
            QT_IS_STATIC="yes"
        fi
        if test x$QT_IS_STATIC = xno ; then
            QT_IS_DYNAMIC=`ls $QTDIR/lib/*.so 2> /dev/null`
            if test "x$QT_IS_DYNAMIC" = x;  then
                AC_MSG_ERROR([*** Couldn't find any Qt libraries])
            fi
        fi
        QT_LIB="-L$QTDIR/bin -lQtCore4 -lQtXml4 -lQtNetwork4 -lws2_32"
        QT_LIB_GUI="-lQtGui4"
        # Check that windres is in path
        AC_PATH_PROGS([WINDRES],[i586-mingw32-windres windres],,[${prefix}/bin:$PATH])
        if test x$WINDRES = x ; then
                AC_MSG_ERROR([*** not found! Make sure you have binutils installed!])
        fi
        ;;
    *)
        if ldconfig -p | fgrep -qe libQtCore.so; then
            QT_IS_DYNAMIC="yes"
            QT_IS_STATIC="no"
        else
            QT_IS_DYNAMIC="no"
            QT_IS_STATIC=`ls $QTDIR/lib/*.a 2> /dev/null`
            if test "x$QT_IS_STATIC" = x; then
                AC_MSG_ERROR([*** Couldn't find any Qt libraries])
            else
                QT_IS_STATIC="yes"
            fi
        fi
        QT_LIB=`pkg-config --libs Qt3Support QtCore QtXml QtNetwork`
        QT_LIB_GUI=`pkg-config --libs QtGui`
        ;;
esac
AC_MSG_CHECKING([if Qt is static])
AC_MSG_RESULT([$QT_IS_STATIC])

QT_LIBS="$QT_LIB"


case "${host}" in
    *irix*)
        QT_LIBS="$QT_LIB"
        if test $QT_IS_STATIC = yes ; then
            QT_LIBS="$QT_LIBS -lXext -lX11 -lm -lSM -lICE"
        fi
        ;;

    *linux*)
        QT_LIBS="$QT_LIB"
        if test $QT_IS_STATIC = yes ; then
            QT_LIBS="$QT_LIBS -lXext -lX11 -lm -lSM -lICE -ldl"
        fi
        ;;

    *osf*)
        # Digital Unix (aka DGUX aka Tru64)
        QT_LIBS="$QT_LIB"
        if test $QT_IS_STATIC = yes ; then
            QT_LIBS="$QT_LIBS -lXext -lX11 -lm -lSM -lICE"
        fi
        ;;

    *solaris*)
        QT_LIBS="$QT_LIB"
        if test $QT_IS_STATIC = yes ; then
            QT_LIBS="$QT_LIBS -lXext -lX11 -lm -lSM -lICE -lresolv -lsocket -lnsl"
        fi
        ;;

esac


QT_CXXFLAGS="$QT_CXXFLAGS -D_REENTRANT -DQT_NO_DEBUG -DQT_THREAD_SUPPORT"

QT_LDADD="$QT_LIBS"

AC_MSG_CHECKING([QT_CXXFLAGS])
AC_MSG_RESULT([$QT_CXXFLAGS])
AC_MSG_CHECKING([QT_LDADD])
AC_MSG_RESULT([$QT_LDADD])

AC_SUBST(QT_CXXFLAGS)
AC_SUBST(QT_LDADD)
AC_SUBST(QT_LIB_GUI)

]) 

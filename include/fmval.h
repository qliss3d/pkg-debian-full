/**************************************************************************
                   fmval.h  -  central computing function
                            -------------------
    begin                : January 03rd 2003
    copyright            : (C) 2003 by Daniel Gruen
    email                : daniel_gruen@web.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// this is a slim and special fms version for qliss3d
// please use the original version from http://sourceforge.net/projects/fmsynth
// if you want to do anything on it

#ifndef _FMVAL_
#define _FMVAL_

#include "fmifstream.h"

#define PI180 0.01745329252	// PI / 180

float   fmval(FMData *f, float atime);

#endif

/**************************************************************************
              vtl.h  -  defining the vtl, pbs and related classes
                             -------------------
    begin                : January 02nd 2003
    copyright            : (C) 2003 by Daniel Gruen
    email                : daniel_gruen@web.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// this is a slim and special fms version for qliss3d
// please use the original version from http://sourceforge.net/projects/fmsynth
// if you want to do anything on it

#ifndef _VTL_
#define _VTL_

class VTL {
	public:
		int getTime(){return time;}
		void setTime(int dtime){time=dtime;}
		unsigned char getValue(){return value;}
		void setValue(unsigned char dvalue){value=dvalue;}
		unsigned char getLinetype(){return linetype;}
		void setLinetype(unsigned char dlinetype){linetype=dlinetype;}
	private:
		int            time    ;
		unsigned char  value   ;
		unsigned char  linetype;
};

#endif


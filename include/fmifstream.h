/**************************************************************************
             fmifstream.h  -  defining fms-file reading utility
                             -------------------
    begin                : April 10th 2002
    copyright            : (C) 2002-2003 by Daniel Gruen
    email                : daniel_gruen@web.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// this is a slim and special fms version for qliss3d
// please use the original version from http://sourceforge.net/projects/fmsynth
// if you want to do anything on it

#ifndef _FMIFSTREAM_
#define _FMIFSTREAM_

#include "vtl.h"

class FMData {
	public:
		FMData(){};
		~FMData(){delete []vtl;}
		void init();

		char * getName(){return name;}
		int    getMode(){return mode;}
		int    getVlen(){return vlen;}
		int    getBytes(){return bytes;}

		void   setMode(int dmode){mode = dmode;}
		void   setVlen(int dvlen){vlen = dvlen;}

		VTL   *vtl ; // Pointer auf das Array der Zeit-Wert-Linientypkomplexe

	protected:
		// float freq ; // Frequenz
		// float time ; // aktueller Zeitindex
		int   mode ; // Modus der Speicherung
		int   nlen ; // L�nge des Namens
		int   vlen ; // Anzahl der Werte
		int   bytes; // Bytes pro Wert
		char * name; // Soundname
		int   i    ;
		unsigned char buf[5];

};

#endif


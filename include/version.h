/**************************************************************************
                      version.h  -  only the version
                            -------------------
    begin                : August 11th 2002
    copyright            : (C) 2002 by Daniel Gruen
    email                : daniel_gruen@web.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// this is a slim and special fms version for qliss3d
// please use the original version from http://sourceforge.net/projects/fmsynth
// if you want to do anything on it

#ifndef fms_version

#define fms_version 0.81
#define debug_level 0 // set this to 1 or 2, recompile, and you've got my debug messages 

#endif

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>QLiss3D</name>
    <message>
        <source>turn figure</source>
        <translation>Figur drehen</translation>
    </message>
    <message>
        <source>reset angle</source>
        <translation>Winkel zurücksetzen</translation>
    </message>
    <message>
        <source>change red color value</source>
        <translation>Farbwert rot ändern</translation>
    </message>
    <message>
        <source>change green color value</source>
        <translation>Farbwert grün ändern</translation>
    </message>
    <message>
        <source>change blue color value</source>
        <translation>Farbwert blau ändern</translation>
    </message>
    <message>
        <source>zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <source>change density</source>
        <translation>Dichte ändern</translation>
    </message>
    <message>
        <source>x phase shift</source>
        <translation>x-Phasenverschiebung</translation>
    </message>
    <message>
        <source>y phase shift</source>
        <translation>y-Phasenverschiebung</translation>
    </message>
    <message>
        <source>z phase shift</source>
        <translation>z-Phasenverschiebung</translation>
    </message>
    <message>
        <source>toggle information</source>
        <translation>Informationen an/aus</translation>
    </message>
    <message>
        <source>cursor keys, mouse dragging</source>
        <translation>Cursortasten, Ziehen mit der Maus</translation>
    </message>
    <message>
        <source>page up / page down</source>
        <translation>Bild auf/ab</translation>
    </message>
    <message>
        <source>r / t</source>
        <translation>r / t</translation>
    </message>
    <message>
        <source>g / h</source>
        <translation>g / h</translation>
    </message>
    <message>
        <source>b / n</source>
        <translation>b / n</translation>
    </message>
    <message>
        <source>+ / -</source>
        <translation>+ / -</translation>
    </message>
    <message>
        <source>Ins / Del</source>
        <translation>Einfg / Entf</translation>
    </message>
    <message>
        <source>y / x</source>
        <translation>y / x</translation>
    </message>
    <message>
        <source>a / s</source>
        <translation>a / s</translation>
    </message>
    <message>
        <source>q / w</source>
        <translation>q / w</translation>
    </message>
    <message>
        <source>i</source>
        <translation>i</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>x freq.</source>
        <translation>x-Freq.</translation>
    </message>
    <message>
        <source>y freq.</source>
        <translation>y-Freq.</translation>
    </message>
    <message>
        <source>z freq.</source>
        <translation>z-Freq.</translation>
    </message>
    <message>
        <source>x angle</source>
        <translation>x-Winkel</translation>
    </message>
    <message>
        <source>y angle</source>
        <translation>y-Winkel</translation>
    </message>
    <message>
        <source>x phase</source>
        <translation>x-Phase</translation>
    </message>
    <message>
        <source>y phase</source>
        <translation>y-Phase</translation>
    </message>
    <message>
        <source>z phase</source>
        <translation>z-Phase</translation>
    </message>
</context>
<context>
    <name>QLiss3DObject</name>
    <message>
        <source>Angle:</source>
        <translation>Winkel:</translation>
    </message>
    <message>
        <source>Points:</source>
        <translation>Punkte:</translation>
    </message>
    <message>
        <source>x-frequency:</source>
        <translation>x-Frequenz:</translation>
    </message>
    <message>
        <source>y-frequency:</source>
        <translation>y-Frequenz:</translation>
    </message>
    <message>
        <source>z-frequency:</source>
        <translation>z-Frequenz:</translation>
    </message>
    <message>
        <source>&amp;Random</source>
        <translation>&amp;Zufall</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Verbinden</translation>
    </message>
    <message>
        <source>&amp;Play</source>
        <translation>&amp;Abspielen</translation>
    </message>
    <message>
        <source>About qliss3d-1.4</source>
        <translation type="obsolete">Über qliss3d-1.4</translation>
    </message>
    <message>
        <source>This is Free Software licensed under the terms and conditions of the Gnu GPL.
Ask me &lt;daniel_gruen@web.de&gt; any questions about qliss3D. Press F1 for help.</source>
        <translation>Dies ist Freie Software, veröffentlicht unter den Bedingungen der Gnu GPL.
Sie können mir &lt;daniel_gruen@web.de&gt; alle Fragen zu qliss3d stellen. Drücken Sie F1 für Hilfe.</translation>
    </message>
    <message>
        <source>About qliss3d-1.3</source>
        <translation type="unfinished">Über qliss3d-1.3 {3d?}</translation>
    </message>
</context>
</TS>

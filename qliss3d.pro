TEMPLATE = app
DEPENDPATH += include
INCLUDEPATH += . include

# Input
HEADERS += qliss3d.h \
           include/fmifstream.h \
           include/fmplayer.h \
           include/fmval.h \
           include/version.h \
           include/vtl.h
SOURCES += fmifstream.cpp fmplayer.cpp fmval.cpp main.cpp qliss3d.cpp

TRANSLATIONS = qliss3d.de.ts \
	       qliss3d.cy.ts

#The following line was inserted by qt3to4
QT += qt3support 
